package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.kirillov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.kirillov.tm.api.service.dto.IUserOwnedServiceDTO;
import ru.tsc.kirillov.tm.dto.model.AbstractWbsModelDTO;
import ru.tsc.kirillov.tm.dto.model.UserDTO;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;
import ru.tsc.kirillov.tm.exception.user.UserNotFoundException;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public abstract class AbstractUserOwnedServiceDTO<M extends AbstractWbsModelDTO, R extends IUserOwnedRepositoryDTO<M>>
        extends AbstractServiceDTO<M, R>
        implements IUserOwnedServiceDTO<M> {
    
    @NotNull
    @Override
    protected abstract IUserOwnedRepositoryDTO<M> getRepository();
    
    @NotNull
    @Autowired
    protected IUserRepositoryDTO userRepository;

    @Nullable
    @Override
    @Transactional
    public M create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        @Nullable final UserDTO user = userRepository.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        return repository.create(user.getId(), name);
    }
    
    @Nullable
    @Override
    @Transactional
    public M create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        @Nullable final UserDTO user = userRepository.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        return repository.create(user.getId(), name, description);
    }
    
    @Nullable
    @Override
    @Transactional
    public M create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        @Nullable final UserDTO user = userRepository.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        return repository.create(user.getId(), name, description, dateBegin, dateEnd);
    }
    
    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        repository.clear(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.findAll(userId, sort.getComparator());
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.findOneByIndex(userId, index);
    }

    @Nullable
    @Override
    @Transactional
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.remove(userId, model);
    }

    @Nullable
    @Override
    @Transactional
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        @Nullable final M result;
        result = repository.removeById(userId, id);
        if (result == null) throw new EntityNotFoundException();
        return result;
    }

    @Nullable
    @Override
    @Transactional
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    @Transactional
    public M update(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.update(model);
    }

    @Nullable
    @Override
    @Transactional
    public M updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        @Nullable final M model;
        model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.update(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public M updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        @Nullable final M model;
        model = repository.findOneByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        repository.update(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public M changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        @Nullable final M model;
        model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.update(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public M changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        @Nullable final M model;
        model = findOneByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        repository.update(model);
        return model;
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnedRepositoryDTO<M> repository = getRepository();
        return repository.count(userId);
    }

}
