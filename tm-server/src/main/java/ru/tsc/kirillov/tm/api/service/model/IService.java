package ru.tsc.kirillov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @Nullable
    M add(@NotNull M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    void clear();

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(final Comparator<M> comparator);

    @NotNull
    List<M> findAll(@Nullable Sort sort);

    boolean existsById(@NotNull String id);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    @Nullable
    M remove(@NotNull M model);

    void removeAll(@Nullable Collection<M> collection);

    @Nullable
    M removeById(@NotNull String id);

    @Nullable
    M removeByIndex(@NotNull Integer index);

    @Nullable
    M update(@NotNull M model);

    long count();

}
