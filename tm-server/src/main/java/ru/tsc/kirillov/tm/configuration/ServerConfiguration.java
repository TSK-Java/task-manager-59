package ru.tsc.kirillov.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.kirillov.tm.api.service.IDatabaseProperty;
import ru.tsc.kirillov.tm.dto.model.AbstractModelDTO;
import ru.tsc.kirillov.tm.model.AbstractModel;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.kirillov.tm")
@PropertySource("classpath:application.properties")
public class ServerConfiguration {

    @NotNull
    private static final String HAZELCAST_USE_LITE_MEMBER = "hibernate.cache.hazelcast.use_lite_member";

    @NotNull
    @Autowired
    private IDatabaseProperty databaseProperty;

    @Bean
    @NotNull
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseProperty.getDatabaseDriver());
        dataSource.setUrl(databaseProperty.getDatabaseUrl());
        dataSource.setUsername(databaseProperty.getDatabaseUserName());
        dataSource.setPassword(databaseProperty.getDatabaseUserPassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan(
                AbstractModelDTO.class.getPackage().getName(),
                AbstractModel.class.getPackage().getName()
        );
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, databaseProperty.getDatabaseDialect());
        properties.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        properties.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());
        properties.put(Environment.FORMAT_SQL, databaseProperty.getDatabaseFormatSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseUseL2Cache());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseProviderConfigFileResourcePath());
        properties.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseRegionFactoryClass());
        properties.put(Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseUserQueryCache());
        properties.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseUseMinimalPuts());
        properties.put(HAZELCAST_USE_LITE_MEMBER, databaseProperty.getDatabaseUseLiteMember());
        properties.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseRegionPrefix());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
