package ru.tsc.kirillov.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.tsc.kirillov.tm.api.repository.model.IProjectRepository;
import ru.tsc.kirillov.tm.model.Project;

@Repository
public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository() {
        super(Project.class);
    }

}
