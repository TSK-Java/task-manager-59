package ru.tsc.kirillov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.AbstractWbsModel;
import ru.tsc.kirillov.tm.model.User;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractWbsModel> extends IRepository<M> {

    @Nullable
    M create(@Nullable User user, @NotNull String name);

    @Nullable
    M create(@Nullable User user, @NotNull String name, @NotNull String description);

    @Nullable
    M create(
            @Nullable User user,
            @NotNull String name,
            @NotNull String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    );

    void clear(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @NotNull Comparator<M> comparator);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    M remove(@Nullable String userId, @Nullable M model);

    @Nullable
    M removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@Nullable String userId, @Nullable Integer index);

    long count(@Nullable String userId);

}
