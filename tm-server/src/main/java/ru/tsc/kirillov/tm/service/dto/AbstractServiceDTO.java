package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.kirillov.tm.api.service.dto.IServiceDTO;
import ru.tsc.kirillov.tm.dto.model.AbstractModelDTO;
import ru.tsc.kirillov.tm.enumerated.Sort;
import ru.tsc.kirillov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kirillov.tm.exception.field.IdEmptyException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@Service
public abstract class AbstractServiceDTO<M extends AbstractModelDTO, R extends IRepositoryDTO<M>> implements IServiceDTO<M> {

    @NotNull
    protected abstract IRepositoryDTO<M> getRepository();

    @Nullable
    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.add(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.add(models);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.set(models);
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        repository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.findAll(sort.getComparator());
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.findAll(comparator);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.findOneByIndex(index);
    }

    @Nullable
    @Override
    @Transactional
    public M remove(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.remove(model);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        repository.removeAll(collection);
    }

    @Nullable
    @Override
    @Transactional
    public M removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        @Nullable final M result;
        result = repository.removeById(id);
        if (result == null) throw new EntityNotFoundException();
        return result;
    }

    @Nullable
    @Override
    @Transactional
    public M removeByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.removeByIndex(index);
    }

    @Nullable
    @Override
    @Transactional
    public M update(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.update(model);
    }

    @Override
    public long count() {
        @NotNull final IRepositoryDTO<M> repository = getRepository();
        return repository.count();
    }

}
