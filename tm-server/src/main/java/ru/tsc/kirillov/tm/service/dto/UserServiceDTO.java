package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.api.repository.dto.IUserRepositoryDTO;
import ru.tsc.kirillov.tm.api.service.IPropertyService;
import ru.tsc.kirillov.tm.api.service.dto.IUserServiceDTO;
import ru.tsc.kirillov.tm.dto.model.UserDTO;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.exception.field.*;
import ru.tsc.kirillov.tm.exception.user.UserNameAlreadyExistsException;
import ru.tsc.kirillov.tm.exception.user.UserNotFoundException;
import ru.tsc.kirillov.tm.util.HashUtil;

@Service
public class UserServiceDTO extends AbstractServiceDTO<UserDTO, IUserRepositoryDTO> implements IUserServiceDTO {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserRepositoryDTO repository;

    @NotNull
    @Override
    protected IUserRepositoryDTO getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new UserNameAlreadyExistsException(login);
        if (isEmailExists(email)) throw new EmailAlreadyExistsException(email);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExists(login)) throw new UserNameAlreadyExistsException(login);
        if (isEmailExists(email)) throw new EmailAlreadyExistsException(email);
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO remove(@Nullable final UserDTO model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(userId);
        if (user == null) return null;
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        update(user);
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final UserDTO user = findOneById(userId);
        if (user == null) return null;
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

}
