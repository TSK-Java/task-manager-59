package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.tsc.kirillov.tm.api.service.dto.IProjectServiceDTO;
import ru.tsc.kirillov.tm.dto.model.ProjectDTO;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;

@Service
public final class ProjectServiceDTO extends AbstractUserOwnedServiceDTO<ProjectDTO, IProjectRepositoryDTO> implements IProjectServiceDTO {

    @NotNull
    @Autowired
    private IProjectRepositoryDTO repository;

    @NotNull
    @Override
    protected IProjectRepositoryDTO getRepository() {
        return repository;
    }

    @NotNull
    @Override
    public String[] findAllId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllId(userId);
    }

}
