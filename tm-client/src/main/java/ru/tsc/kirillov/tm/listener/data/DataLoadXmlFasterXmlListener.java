package ru.tsc.kirillov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.DataXmlFasterXmlLoadRequest;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class DataLoadXmlFasterXmlListener extends AbstractDataListener {

    @NotNull
    @Override
    public String getName() {
        return "data-load-xml-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузить состояние приложения из xml файла (FasterXML API)";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataLoadXmlFasterXmlListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Загрузка состояния приложения из xml файла (FasterXML API)]");
        getDomainEndpoint().loadDataXmlFasterXml(new DataXmlFasterXmlLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
