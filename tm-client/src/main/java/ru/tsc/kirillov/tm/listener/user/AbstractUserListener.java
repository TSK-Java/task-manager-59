package ru.tsc.kirillov.tm.listener.user;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.IUserEndpoint;
import ru.tsc.kirillov.tm.listener.AbstractListener;

@Getter
@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

}
