package ru.tsc.kirillov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.request.ProjectClearRequest;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class ProjectClearListener extends AbstractProjectTaskListener {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удалить все проекты.";
    }

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[Очистка списка проектов]");
        getProjectTaskEndpoint().clearProject(new ProjectClearRequest(getToken()));
    }

}
