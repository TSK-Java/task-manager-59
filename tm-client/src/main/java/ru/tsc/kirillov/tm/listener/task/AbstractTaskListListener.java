package ru.tsc.kirillov.tm.listener.task;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.dto.model.TaskDTO;
import ru.tsc.kirillov.tm.exception.entity.TaskNotFoundException;

import java.util.List;

@Component
public abstract class AbstractTaskListListener extends AbstractTaskListener {

    protected void printTask(@Nullable final List<TaskDTO> tasks) {
        if (tasks == null) throw new TaskNotFoundException();
        int idx = 0;
        for(final TaskDTO task: tasks) {
            if (task == null) continue;
            System.out.println(++idx + ". " + task);
        }
    }

}
